using NHibernate.SimpleMapping.Generators;
using System;
using System.Collections.Generic;
using Xunit;

namespace NHibernate.SimpleMapping.Tests
{
    public class IdentityGeneratorTests
    {
        [Fact]
        public void GuidGeneratedWebHashesAreTheSame()
        {
            var randomGuid = Guid.NewGuid();

            var webHash1 = IdentityGenerator.WebHash(randomGuid);
            var webHash2 = IdentityGenerator.WebHash(randomGuid);

            Assert.Equal(webHash1, webHash2);
        }

        [Fact]
        public void WebHashIsUnique()
        {
            var webHashes = new HashSet<string>();

            for (int i = 0; i < 1000000; i++)
            {
                webHashes.Add(IdentityGenerator.WebHash());
            }

            Assert.Equal(1000000, webHashes.Count);
        }
    }
}
