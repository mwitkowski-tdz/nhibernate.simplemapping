﻿using NHibernate.Proxy;
using NHibernate.SimpleMapping.Attributes;
using NHibernate.SimpleMapping.Generators;
using NHibernate.SimpleMapping.Interfaces;
using System;

namespace NHibernate.SimpleMapping.Tests.Classes
{
    public abstract class BaseEntity : IEntity
    {
        [Key(typeof(WebHashValueGeneratorDef))]
        [Ansi]
        [Length(16)]
        public string Id { get; set; }

        [Ansi]
        [Length(150)]
        public string CreatedBy { get; set; }

        [Ansi]
        [Length(150)]
        public string UpdatedBy { get; set; }

        [UtcDateTime(NoMs = true)]
        public DateTime CreatedDate { get; set; }

        [UtcDateTime(NoMs = true)]
        public DateTime UpdatedDate { get; set; }

        [Version]
        public int Version { get; set; }

        [Default("false")]
        public bool IsDeleted { get; protected set; }

        public void SetDeleted(bool isDeleted = true)
        {
            IsDeleted = isDeleted;
        }

        public override bool Equals(object obj)
        {
            // https://stackoverflow.com/questions/5002670/lazy-loaded-nhibernate-properties-in-equals-and-gethashcode

            if (obj == null)
                return false;

            if (ReferenceEquals(obj, this))
                return true;

            var otherType = NHibernateProxyHelper.GetClassWithoutInitializingProxy(obj);
            var thisType = NHibernateProxyHelper.GetClassWithoutInitializingProxy(this);
            if (!otherType.Equals(thisType))
                return false;

            var other = obj as BaseEntity;
            if (other == null)
                return false;

            // At least one of the objects is new (Id is empty)
            if (string.IsNullOrEmpty(Id) || string.IsNullOrEmpty(other.Id))
                return false;
            else
                return other.Id.Equals(Id) && other.Version.Equals(Version);
        }

        public override int GetHashCode()
        {
            if (!string.IsNullOrEmpty(Id))
                return Id.GetHashCode();
            else
                return base.GetHashCode();
        }
    }
}
