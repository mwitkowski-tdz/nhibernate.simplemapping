﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernate.SimpleMapping
{
    internal class JoinedMapBuilder<TEntity> : JoinedSubclassMapping<TEntity> where TEntity : class
    {
        public JoinedMapBuilder()
        {
            this.MapJoinedClass();
        }
    }
}
