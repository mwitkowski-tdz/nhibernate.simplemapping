﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernate.SimpleMapping
{
    internal class MapBuilder<TEntity> : ClassMapping<TEntity> where TEntity : class
    {
        public MapBuilder()
        {
            this.MapClass();
        }
    }
}
