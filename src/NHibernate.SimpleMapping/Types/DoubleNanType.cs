﻿using NHibernate.Criterion;
using NHibernate.Engine;
using NHibernate.SimpleMapping.Interfaces;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using System;
using System.Data;
using System.Data.Common;
using System.Runtime.Serialization;

namespace NHibernate.SimpleMapping.Types
{
    [Serializable]
    public class DoubleNanType : IUserType
    {
        public DoubleNanType()
        {
        }

        public SqlType[] SqlTypes => new[] { NHibernateUtil.Double.SqlType };

        public System.Type ReturnedType => typeof(double?);

        public bool IsMutable => false;

        public object Assemble(object cached, object owner)
        {
            return DeepCopy(cached);
        }

        public object DeepCopy(object value)
        {
            if (value == null)
                return null;
            if (value is double val)
            {
                if (double.IsNaN(val))
                    return null;
                return val;
            }
            if (value.GetType() == typeof(double?))
            {
                double? @double = (double?)value;
                if (double.IsNaN(@double.Value)) return null;
                return @double;
            }
            return value;
        }

        public object Disassemble(object value)
        {
            return DeepCopy(value);
        }

        public new bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y))
                return true;

            if (x == null || y == null)
                return false;

            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

      

        public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            var value = (double?)NHibernateUtil.Double.NullSafeGet(rs, names, session, owner);

             double? @double = (double?)value;
                if (@double.HasValue && double.IsNaN(@double.Value)) return null;
                return @double;
            
        }

        public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            if (value == null || double.IsNaN ((double)value))
                ((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
            else
                ((IDataParameter)cmd.Parameters[index]).Value = value;
        }

        public object Replace(object original, object target, object owner)
        {
            return DeepCopy(original);
        }
    }
}
