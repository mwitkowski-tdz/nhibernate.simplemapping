﻿using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace NHibernate.SimpleMapping.Types
{
    public class AnsiStringListUserType<TList, TElement> : StringListUserType<TList, TElement> where TList : IEnumerable<TElement>
    {
        public AnsiStringListUserType() : base(true)
        {

        }
    }

}
