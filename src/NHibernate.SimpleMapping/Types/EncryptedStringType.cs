﻿using NHibernate.Engine;
using NHibernate.SimpleMapping.Interfaces;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using System;
using System.Data;
using System.Data.Common;
using System.Runtime.Serialization;

namespace NHibernate.SimpleMapping.Types
{
    [Serializable]
    public class EncryptedStringType : IUserType, ISerializable
    {
        public EncryptedStringType()
        {
        }

        public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            //treat for the posibility of null values
            var encryptor = GetEncriptor(session);
            object encryptedValue = NHibernateUtil.String.NullSafeGet(rs, names[0], session);
            return encryptedValue != null
                       ? encryptor.Decrypt((string)encryptedValue)
                       : null;
        }

        public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            if (value == null)
            {
                NHibernateUtil.String.NullSafeSet(cmd, null, index, session);
                return;
            }

            var encryptor = GetEncriptor(session);
            string encryptedValue = encryptor.Encrypt((string)value);
            NHibernateUtil.String.NullSafeSet(cmd, encryptedValue, index, session);
        }

        public IEncryptor GetEncriptor(ISessionImplementor session)
        {
            var serviceProvider = session.Interceptor as IServiceProvider;
            if (serviceProvider == null)
                throw new Exception("Cannot get IEncryptor because the Interceptor does not implement IServiceProvider.");

            var encryptor = (IEncryptor)serviceProvider.GetService(typeof(IEncryptor));
            if (encryptor == null)
                throw new Exception("Cannot get IEncryptor service because the ServiceProvider does not contain it.");

            return encryptor;
        }

        public object DeepCopy(object value)
        {
            return value == null
                       ? null
                       : string.Copy((string)value);
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public object Assemble(object cached, object owner)
        {
            return DeepCopy(cached);
        }

        public object Disassemble(object value)
        {
            return DeepCopy(value);
        }

        public SqlType[] SqlTypes
        {
            get { return new[] { new SqlType(DbType.String) }; }
        }

        public System.Type ReturnedType
        {
            get { return typeof(string); }
        }

        public bool IsMutable
        {
            get { return false; }
        }

        public new bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y))
                return true;
            if (x == null || y == null)
                return false;
            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            if (x == null)
                throw new ArgumentNullException("x");
            return x.GetHashCode();
        }

        /// <summary>
        /// Serialize EncryptedStringSerializationHelper instead
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.SetType(typeof(EncryptedStringSerializationHelper));
        }

        [Serializable]
        internal sealed class EncryptedStringSerializationHelper : IObjectReference
        {
            /// <summary>
            /// Returns a new EncryptedString instance
            /// </summary>
            /// <param name="context"></param>
            /// <returns></returns>
            /// <remarks>Avoid serializing the encryption stuff</remarks>
            public object GetRealObject(StreamingContext context)
            {
                return new EncryptedStringType();
            }

        }

    }
}
