﻿using NHibernate.Mapping.ByCode;
using System;
using System.Collections.Generic;
using System.Text;

namespace NHibernate.SimpleMapping.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FetchAttribute : Attribute
    {
        public FetchAttribute(NHibernate.SimpleMapping.Enums.FetchMode mode)
        {
            Mode = mode;
        }

        public NHibernate.SimpleMapping.Enums.FetchMode Mode { get; protected set; }
    }

   
}
