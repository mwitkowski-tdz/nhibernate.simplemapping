﻿using NHibernate.Mapping.ByCode;
using System;

namespace NHibernate.SimpleMapping.Attributes
{
    /// <summary>
    /// Mark property as Identifier. Generator is a NHibernate.Mapping.ByCode.IGeneratorDef type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class KeyAttribute : Attribute
    {
        public KeyAttribute()
        {
        }

        public KeyAttribute(System.Type generator)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");

            if (!typeof(IGeneratorDef).IsAssignableFrom(generator))
                throw new Exception("Generator must inherit from NHibernate.Mapping.ByCode.IGeneratorDef.");

            Generator = generator;
        }

        public System.Type Generator { get; protected set; }
    }
}
