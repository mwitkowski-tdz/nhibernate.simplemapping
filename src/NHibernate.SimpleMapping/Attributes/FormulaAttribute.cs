﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NHibernate.SimpleMapping.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FormulaAttribute : Attribute
    {
        public FormulaAttribute(string formula)
        {
            Formula = formula;
        }

        public string Formula { get; protected set; }
    }
}
