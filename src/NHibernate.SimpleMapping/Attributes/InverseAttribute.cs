﻿using System;

namespace NHibernate.SimpleMapping.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class InverseAttribute : Attribute
    {
    }
}
