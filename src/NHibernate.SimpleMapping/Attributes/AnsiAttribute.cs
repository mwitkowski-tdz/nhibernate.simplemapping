﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NHibernate.SimpleMapping.Attributes
{
    /// <summary>
    /// Ansi String
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class AnsiAttribute : Attribute
    {
    }

}
