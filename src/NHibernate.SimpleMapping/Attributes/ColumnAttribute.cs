﻿using System;

namespace NHibernate.SimpleMapping.Attributes
{
    /// <summary>
    /// Specify column name if it is different from the property name depends on your naming strategy
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {
        public ColumnAttribute(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Column name or Property name
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Used only for many-to-many relation
        /// </summary>
        public string OtherName { get; set; }
    }
}
