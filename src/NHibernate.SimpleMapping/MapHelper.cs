﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.SimpleMapping.Attributes;
using NHibernate.SimpleMapping.Types;
using NHibernate.Type;
using NHibernate.UserTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate.SimpleMapping.Factors;
using System.Reflection;
using NHibernate.SimpleMapping.Enums;
using NHibernate.Criterion;

namespace NHibernate.SimpleMapping
{
	internal static class MapHelper
	{
		private const string DefaultDiscriminatorColumnName = "Discriminator";

		public static void MapClass<TEntity>(this ClassMapping<TEntity> mapper, bool useAssignedKeyGen = false) where TEntity : class
		{
			var type = typeof(TEntity);

			// Do not map. So it means that it is mapped using a traditional method of mapping. But it still can be used
			// as a reference if it has a [Table] attribute.
			if (type.GetCustomAttribute<NotMappedAttribute>(false) != null)
				return;

			#region Table

			var table = type.GetCustomAttribute<TableAttribute>();
			if (table == null)
				throw new NotSupportedException($"Cannot create mapping from class '{type.Name}' without [Table] attribute.");

			// Table name
			mapper.Table(table.Name ?? type.Name);

			// Schema
			if (!string.IsNullOrEmpty(table.Schema))
				mapper.Schema(table.Schema);

			// Lazy
			if (type.GetCustomAttribute<NotLazyAttribute>() != null)
				mapper.Lazy(false);

			// Discriminator
			if (type.GetCustomAttribute<DiscriminatorAttribute>() != null)
			{
				var discriminator = type.GetCustomAttribute<DiscriminatorAttribute>();
				mapper.Discriminator(map =>
				{
					map.Length(50);
					map.NotNullable(true);
					map.Type(TypeFactory.GetAnsiStringType(50));
					map.Column(discriminator.ColumnName ?? DefaultDiscriminatorColumnName);
				});

				// Read only discriminator columns
				var discriminatorProps = type.GetProperties(BindingFlags.Instance | BindingFlags.Public)
					.Where(x => x.GetCustomAttribute<DiscriminatorAttribute>() != null).ToArray();

				foreach (var discriminatorProp in discriminatorProps)
				{
					var discriminatorAttr = discriminatorProp.GetCustomAttribute<DiscriminatorAttribute>();
					mapper.Property(discriminatorProp.Name, map =>
					{
						map.Length(50);
						map.NotNullable(true);
						map.Type(FuncHelper.GetNHibernateType(discriminatorProp.PropertyType, discriminatorProp));
						map.Column(discriminatorAttr.ColumnName ?? DefaultDiscriminatorColumnName);
						map.Access(Accessor.None);
						map.Update(false);
						map.Insert(false);
					});
				}
			}
			else
			{
				if (type.IsAbstract)
					throw new Exception($"Mapped class '{type.Name}' must not be abstract.");
			}

			#endregion Table

			var props = FuncHelper.GetPublicVirtualProperties(type);
			foreach (var prop in props)
			{
				if (prop.GetCustomAttribute<KeyAttribute>() != null)
				{
					var propType = prop.PropertyType.IsGenericType ? prop.PropertyType.GetGenericArguments()[0] : prop.PropertyType;
					var columnName = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
					var nhType = FuncHelper.GetNHibernateType(propType, prop);
					var nhLength = FuncHelper.GetNHibernateLength(propType, prop);

					var generator = prop.GetCustomAttribute<KeyAttribute>()?.Generator;

					mapper.Id(prop.Name, map =>
					{
						map.Column(cm =>
						{
							if (columnName != null)
								cm.Name(columnName);
						});
						map.Type((IIdentifierType)nhType);
						if (nhLength > 0)
							map.Length(nhLength);
						if (generator != null)
							map.Generator(useAssignedKeyGen ? NHibernate.Mapping.ByCode.Generators.Assigned : (IGeneratorDef)Activator.CreateInstance(generator));
					});
				}
				else if (prop.GetCustomAttribute<VersionAttribute>() != null)
				{
					var propType = prop.PropertyType.IsGenericType ? prop.PropertyType.GetGenericArguments()[0] : prop.PropertyType;
					var columnName = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
					var nhType = FuncHelper.GetNHibernateType(propType, prop);

					mapper.Version(prop.Name, map =>
					{
						map.Column(cm =>
						{
							if (columnName != null)
								cm.Name(columnName);
						});
						map.Type((IVersionType)nhType);
					});
				}
				else
				{
					mapper.MapProperty(type, prop);
				}
			}
		}

		public static void MapSubClass<TEntity>(this SubclassMapping<TEntity> mapper) where TEntity : class
		{
			var type = typeof(TEntity);

			// Do not map. So it means that it is mapped using a traditional method of mapping. But it still can be used
			// as a reference if it has a [Table] attribute.
			if (type.GetCustomAttribute<NotMappedAttribute>(false) != null)
				return;

			if (type.IsAbstract)
				throw new Exception($"Mapped class '{type.Name}' must not be abstract.");

			mapper.DiscriminatorValue(type.Name);

			var props = FuncHelper.GetPublicVirtualProperties(type, excludeInherited: true);
			foreach (var prop in props)
			{
				if (prop.GetCustomAttribute<KeyAttribute>() != null)
				{
					// It should not happen
				}
				else if (prop.GetCustomAttribute<VersionAttribute>() != null)
				{
					// It should not happen
				}
				else
				{
					mapper.MapProperty(type, prop);
				}
			}
		}

		public static void MapSubJoinedClass<TEntity>(this SubclassMapping<TEntity> mapper) where TEntity : class
		{
			var type = typeof(TEntity);

			// Do not map. So it means that it is mapped using a traditional method of mapping. But it still can be used
			// as a reference if it has a [Table] attribute.
			if (type.GetCustomAttribute<NotMappedAttribute>(false) != null)
				return;

			if (type.IsAbstract)
				throw new Exception($"Mapped class '{type.Name}' must not be abstract.");

			mapper.DiscriminatorValue(type.Name);

			// Lazy
			if (type.GetCustomAttribute<NotLazyAttribute>() != null)
				mapper.Lazy(false);

			var table = type.GetCustomAttribute<TableAttribute>();
			if (table == null)
				throw new Exception($"Cannot create mapping from class '{type.Name}' without [Table] attribute.");

			var tableName = table.Name ?? type.Name;

			mapper.Join(tableName, jmapper =>
			{
				// Table name
				jmapper.Table(tableName);

				// Schema
				if (!string.IsNullOrEmpty(table.Schema))
					jmapper.Schema(table.Schema);

				// Joined Key
				jmapper.Key(key =>
				{
					key.Column(cm =>
					{
						cm.Name(table.JoinedKey ?? type.BaseType.Name + "Id");
					});
					key.NotNullable(true);
					key.OnDelete(OnDeleteAction.Cascade);
					key.Unique(true);
					key.Update(true);
				});

				var props = FuncHelper.GetPublicVirtualProperties(type, excludeInherited: true);
				foreach (var prop in props)
				{
					if (prop.GetCustomAttribute<KeyAttribute>() != null)
					{
						// It should not happen
					}
					else if (prop.GetCustomAttribute<VersionAttribute>() != null)
					{
						// It should not happen
					}
					else
					{
						jmapper.MapProperty(type, prop);
					}
				}
			});
		}

		public static void MapJoinedClass<TEntity>(this JoinedSubclassMapping<TEntity> mapper) where TEntity : class
		{
			var type = typeof(TEntity);

			// Do not map. So it means that it is mapped using a traditional method of mapping (xml or bycode). But it
			// still can be used as a reference if it has a [Table] attribute.
			if (type.GetCustomAttribute<NotMappedAttribute>(false) != null)
				return;

			if (type.IsAbstract)
				throw new Exception($"Joined mapped class '{type.Name}' must not be abstract.");

			var table = type.GetCustomAttribute<TableAttribute>();
			if (table == null)
				throw new Exception($"Cannot create mapping from class '{type.Name}' without [Table] attribute.");

			// Table name
			mapper.Table(table.Name ?? type.Name);

			// Schema
			if (!string.IsNullOrEmpty(table.Schema))
				mapper.Schema(table.Schema);

			// Lazy
			if (type.GetCustomAttribute<NotLazyAttribute>() != null)
				mapper.Lazy(false);

			// Joined Key
			mapper.Key(key =>
			{
				key.Column(cm =>
				{
					cm.Name(table.JoinedKey ?? type.BaseType.Name + "Id");
				});
				key.NotNullable(true);
				key.OnDelete(OnDeleteAction.Cascade);
				key.Unique(true);
				key.Update(true);
			});

			var props = FuncHelper.GetPublicVirtualProperties(type, excludeInherited: true);
			foreach (var prop in props)
			{
				if (prop.GetCustomAttribute<KeyAttribute>() != null)
				{
					// It should not happen
				}
				else if (prop.GetCustomAttribute<VersionAttribute>() != null)
				{
					// It should not happen
				}
				else
				{
					mapper.MapProperty(type, prop);
				}
			}
		}

		public static void MapProperty<TEntity>(this IMinimalPlainPropertyContainerMapper<TEntity> mapper, System.Type type, PropertyInfo prop) where TEntity : class
		{
			// Ignore Property
			if (prop.GetCustomAttribute<NotMappedAttribute>() != null)
				return;

			var baseType = prop.PropertyType.IsGenericType ? prop.PropertyType.GetGenericArguments()[0] : prop.PropertyType;
			var genericTypeDef = prop.PropertyType.IsGenericType ? prop.PropertyType.GetGenericTypeDefinition() : null;
			var genericTypeArg = prop.PropertyType.IsGenericType ? prop.PropertyType.GetGenericArguments()[0] : null;
			var isNullable = genericTypeDef == typeof(Nullable<>);
			var isString = baseType == typeof(string);
			var formula = prop.GetCustomAttribute<FormulaAttribute>()?.Formula;
			var isRequired = prop.GetCustomAttribute<NotNullAttribute>() != null;
			var isProperty = (baseType.IsValueType || isString) && (genericTypeDef == null || isNullable);
			//var isElement = (baseType.IsEnum || isString) && genericTypeDef != null && FuncHelper.IsGenericTypeOf(prop.PropertyType, typeof(IEnumerable<>)) && prop.GetCustomAttribute<TableAttribute>() != null;
			var isStringList = (baseType.IsEnum || isString) && genericTypeDef != null && FuncHelper.IsGenericTypeOf(prop.PropertyType, typeof(IEnumerable<>)) && prop.GetCustomAttribute<TableAttribute>() == null;

			if (isProperty || isStringList) // Property
			{
				#region Property

				var columnName = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
				var nhType = FuncHelper.GetNHibernateType(baseType, prop);
				var nhLength = FuncHelper.GetNHibernateLength(baseType, prop, isStringList ? (int?)2000 : null);
				var isLazy = prop.GetCustomAttribute<LazyAttribute>() != null ? true : false;
				var defaultValue = FuncHelper.GetNHibernateDefaultValue(baseType, prop, isString);
				var isUnique = prop.GetCustomAttribute<UniqueAttribute>() != null;
				var readOnly = prop.GetCustomAttribute<ReadOnlyAttribute>() != null;

				mapper.Property(prop.Name, map =>
				{
					map.Column(cm =>
					{
						if (columnName != null)
							cm.Name(columnName);
						if (defaultValue != null)
							cm.Default(defaultValue);
						if (isRequired || (!isString && !isNullable && !isStringList))
							cm.NotNullable(true);
						if (isUnique)
							cm.Unique(true);
						if (nhLength > 0)
							cm.Length(nhLength);
					});
					if (baseType == typeof(string) && prop.GetCustomAttribute<EncryptedAttribute>() != null)
						map.Type<EncryptedStringType>();
					else if (isStringList)
						map.Type(
							(baseType.IsEnum || prop.GetCustomAttribute<AnsiAttribute>() != null ? typeof(AnsiStringListUserType<,>) : typeof(StringListUserType<,>))
								.MakeGenericType(prop.PropertyType, baseType)
							, null);
					else if (prop.PropertyType == typeof(double?)) 
						map.Type<DoubleNanType>();
					else 
						map.Type(nhType);
					if (isRequired || (!isString && !isNullable && !isStringList))
						map.NotNullable(true);
					if (nhLength > 0)
						map.Length(nhLength);
					map.Unique(isUnique);
					map.Lazy(isLazy);
					if (!string.IsNullOrEmpty(formula))
						map.Formula(formula);
					if (readOnly)
					{
						map.Access(Accessor.ReadOnly);
						map.Insert(true);
						map.Update(true);
					}
				});

				#endregion Property
			}
			else if (baseType.IsClass && genericTypeDef == null) // Reference
			{
				#region Reference

				var refTable = baseType.GetCustomAttribute<TableAttribute>();
				if (refTable != null)
				{
					var columnName = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
					var isNotLazy = prop.GetCustomAttribute<NotLazyAttribute>() != null ? true : false;
					var oneToOne = prop.GetCustomAttribute<OneToOneAttribute>();
					var cascade = prop.GetCustomAttribute<CascadeAttribute>();

					if (oneToOne != null)
					{
						typeof(MapHelper).GetMethod("MapOne")
							.MakeGenericMethod(typeof(TEntity), baseType)
							.Invoke(null, new object[] {
								mapper,
								prop.Name,
								oneToOne.OtherSidePropertyName,
								cascade?.Cascade,
								isNotLazy ? false : true,
								formula
							});
					}
					else
					{
						var isUnique = prop.GetCustomAttribute<UniqueAttribute>() != null;

						mapper.GetType()
							.GetMethod("ManyToOne", new[] { typeof(string), typeof(Action<IManyToOneMapper>) })
							.MakeGenericMethod(baseType)
							.Invoke(mapper, new object[] {
								prop.Name,
								new Action<IManyToOneMapper>(map =>
								{
									if (columnName != null)
										map.Column(columnName);
									else
										map.Column(prop.Name + "Id");
									if (cascade != null)
										map.Cascade(cascade.Cascade);
									if (formula != null)
										map.Formula(formula);
									map.Unique(isUnique);
									map.NotNullable(isRequired);
									if (isNotLazy)
										map.Lazy(LazyRelation.NoLazy);
								})
							});
					}
				}

				#endregion Reference
			}
			else if (genericTypeDef != null && FuncHelper.IsGenericTypeOf(prop.PropertyType, typeof(IList<>))) // List or Element
			{
				#region List

				typeof(MapHelper).GetMethod("MapList")
					.MakeGenericMethod(typeof(TEntity), genericTypeArg)
					.Invoke(null, new object[] {
						mapper,
						prop
					});

				#endregion List
			}
			else if (genericTypeDef != null && FuncHelper.IsGenericTypeOf(prop.PropertyType, typeof(ISet<>))) // Set or Element
			{
				#region Set

				typeof(MapHelper).GetMethod("MapSet")
					.MakeGenericMethod(typeof(TEntity), genericTypeArg)
					.Invoke(null, new object[] {
						mapper,
						prop
					});

				#endregion Set
			}
			else if (genericTypeDef != null && FuncHelper.IsGenericTypeOf(prop.PropertyType, typeof(IEnumerable<>))) // Bag or Element
			{
				#region Bag

				typeof(MapHelper).GetMethod("MapBag")
					.MakeGenericMethod(typeof(TEntity), genericTypeArg)
					.Invoke(null, new object[] {
						mapper,
						prop
					});

				#endregion Bag
			}
		}

		public static void MapOne<TEntity, TElement>(this IPlainPropertyContainerMapper<TEntity> mapper, string notVisiblePropertyOrFieldName, string otherSidePropertyName, Cascade? cascade, bool lazy, string formula) where TEntity : class where TElement : class
		{
			var propType = typeof(TElement);
			var entityType = typeof(TEntity);

			MemberInfo otherSideMember;
			if (string.IsNullOrEmpty(otherSidePropertyName))
			{
				var foundTypes = FuncHelper.GetPublicVirtualProperties(propType).Where(x => x.PropertyType == entityType).ToArray();
				if (foundTypes.Length == 0)
					throw new Exception($"Property on the other side of type '{entityType.Name}' in class '{propType.Name}' not found for one-to-one relation of '{entityType.Name}.{notVisiblePropertyOrFieldName}'.");
				if (foundTypes.Length > 1)
					throw new Exception($"Property on the other side of type '{entityType.Name}' in class '{propType.Name}' found more the one time for one-to-one relation of '{entityType.Name}.{notVisiblePropertyOrFieldName}'.");

				otherSideMember = foundTypes[0];
			}
			else
			{
				otherSideMember = propType.GetPropertyOrFieldMatchingName(otherSidePropertyName);
				if (otherSideMember.DeclaringType != entityType)
					throw new Exception($"The property '{propType}.{otherSidePropertyName}' must be of {entityType.Name} type for one-to-one relation of '{entityType.Name}.{notVisiblePropertyOrFieldName}'.");
			}

			if (propType.GetCustomAttribute<TableAttribute>() != null && propType.GetCustomAttribute<NotMappedAttribute>() == null && otherSideMember.GetCustomAttribute<UniqueAttribute>() == null)
				throw new Exception($"The property '{propType}.{otherSidePropertyName}' must have [Unique] attribute for one-to-one relation of '{entityType.Name}.{notVisiblePropertyOrFieldName}'.");

			mapper.OneToOne<TElement>(notVisiblePropertyOrFieldName, map =>
			{
				map.PropertyReference(otherSideMember);
				if (cascade.HasValue)
					map.Cascade(cascade.Value);
				map.Lazy(lazy ? LazyRelation.Proxy : LazyRelation.NoLazy);
				if (!string.IsNullOrEmpty(formula))
					map.Formula(formula);
			});
		}

		public static void MapList<TEntity, TElement>(this ICollectionPropertiesContainerMapper<TEntity> mapper, PropertyInfo prop) where TEntity : class
		{
			var notVisiblePropertyOrFieldName = prop.Name;
			var columnNameOrPropertyRef = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
			var manyToManyTableName = prop.GetCustomAttribute<TableAttribute>()?.Name;
			var otherColumnName = prop.GetCustomAttribute<ColumnAttribute>()?.OtherName;
			var cascade = prop.GetCustomAttribute<CascadeAttribute>()?.Cascade;
			var fetchMode = prop.GetCustomAttribute<FetchAttribute>()?.Mode;
			var lazy = prop.GetCustomAttribute<NotLazyAttribute>() != null ? false : true;
			var inverse = prop.GetCustomAttribute<InverseAttribute>() != null ? true : false;
			var formula = prop.GetCustomAttribute<FormulaAttribute>()?.Formula;

			// Find column name if it's Property name or empty
			columnNameOrPropertyRef = FuncHelper.FindReferencedColumnName(typeof(TEntity), typeof(TElement), columnNameOrPropertyRef);

			if (!string.IsNullOrEmpty(columnNameOrPropertyRef) && string.IsNullOrEmpty(manyToManyTableName) && string.IsNullOrEmpty(otherColumnName))
			{
				// One To Many
				if (typeof(TElement).IsValueType || typeof(TElement) == typeof(string))
					throw new Exception($"The property {columnNameOrPropertyRef} with OneToMany cannot contain simple type.");

				mapper.List(notVisiblePropertyOrFieldName, new Action<IListPropertiesMapper<TEntity, TElement>>(map =>
				{
					map.Key(k => k.Column(columnNameOrPropertyRef));
					if (cascade.HasValue)
						map.Cascade(cascade.Value);
					map.Lazy(lazy ? CollectionLazy.Lazy : CollectionLazy.NoLazy);
					map.Fetch(FetchFactor.Get(fetchMode));	
                    map.Inverse(inverse);
				}), action => action.OneToMany());
			}
			else if (!string.IsNullOrEmpty(manyToManyTableName))
			{
				// Many To Many
				if (string.IsNullOrEmpty(columnNameOrPropertyRef))
					columnNameOrPropertyRef = typeof(TEntity).Name + "Id";
				if (string.IsNullOrEmpty(otherColumnName))
					otherColumnName = typeof(TElement).Name + "Id";

				mapper.List(
					notVisiblePropertyOrFieldName,
					new Action<IListPropertiesMapper<TEntity, TElement>>(map =>
					{
						map.Key(k => k.Column(columnNameOrPropertyRef));
						if (cascade.HasValue)
							map.Cascade(cascade.Value);
						map.Lazy(lazy ? CollectionLazy.Lazy : CollectionLazy.NoLazy);
						map.Inverse(inverse);
						map.Table(manyToManyTableName);
					}),
					new Action<ICollectionElementRelation<TElement>>(map =>
					{
						if (typeof(TElement) == typeof(string) )
						{
							map.Element(m =>
							{
								m.Column(otherColumnName);
								m.Formula(formula);
								m.Type((IType)NHibernateUtil.AnsiString);
								m.Length(50);
							});
                        }
                        else if (typeof(TElement).IsEnum)
                        {
                            map.Element(m =>
                            {
                                m.Column(otherColumnName);
                                m.Formula(formula);
                                m.Type<EnumStringType<TElement>>();
                                m.Length(50);
                            });
                        }
                        else
						{
							map.ManyToMany(m =>
							{
								m.Column(otherColumnName);
								m.Formula(formula);
							});
						}
					})
				);
			}
		}

		public static void MapSet<TEntity, TElement>(this ICollectionPropertiesContainerMapper<TEntity> mapper, PropertyInfo prop)
		{
			var notVisiblePropertyOrFieldName = prop.Name;
			var columnNameOrPropertyRef = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
			var manyToManyTableName = prop.GetCustomAttribute<TableAttribute>()?.Name;
			var otherColumnName = prop.GetCustomAttribute<ColumnAttribute>()?.OtherName;
			var cascade = prop.GetCustomAttribute<CascadeAttribute>()?.Cascade;
            var fetchMode = prop.GetCustomAttribute<FetchAttribute>()?.Mode;
            var lazy = prop.GetCustomAttribute<NotLazyAttribute>() != null ? false : true;
			var inverse = prop.GetCustomAttribute<InverseAttribute>() != null ? true : false;
			var formula = prop.GetCustomAttribute<FormulaAttribute>()?.Formula;

			columnNameOrPropertyRef = FuncHelper.FindReferencedColumnName(typeof(TEntity), typeof(TElement), columnNameOrPropertyRef);

			if (!string.IsNullOrEmpty(columnNameOrPropertyRef) && string.IsNullOrEmpty(manyToManyTableName) && string.IsNullOrEmpty(otherColumnName))
			{
				// One To Many
				if (typeof(TElement).IsValueType || typeof(TElement) == typeof(string))
					throw new Exception($"The property {columnNameOrPropertyRef} with OneToMany cannot contain simple type.");

				mapper.Set(notVisiblePropertyOrFieldName, new Action<ISetPropertiesMapper<TEntity, TElement>>(map =>
				{
					map.Key(k => k.Column(columnNameOrPropertyRef));
					if (cascade.HasValue)
						map.Cascade(cascade.Value);
					map.Lazy(lazy ? CollectionLazy.Lazy : CollectionLazy.NoLazy);
					map.Inverse(inverse);
                    map.Fetch(FetchFactor.Get(fetchMode));
                }), action => action.OneToMany());
			}
			else if (!string.IsNullOrEmpty(manyToManyTableName))
			{
				// Many To Many
				if (string.IsNullOrEmpty(columnNameOrPropertyRef))
					columnNameOrPropertyRef = typeof(TEntity).Name + "Id";
				if (string.IsNullOrEmpty(otherColumnName))
					otherColumnName = typeof(TElement).Name + "Id";

				mapper.Set(
					notVisiblePropertyOrFieldName,
					new Action<ISetPropertiesMapper<TEntity, TElement>>(map =>
					{
						map.Key(k => k.Column(columnNameOrPropertyRef));
						if (cascade.HasValue)
							map.Cascade(cascade.Value);
						map.Lazy(lazy ? CollectionLazy.Lazy : CollectionLazy.NoLazy);
						map.Inverse(inverse);
						map.Table(manyToManyTableName);
					}),
					new Action<ICollectionElementRelation<TElement>>(map =>
					{
						if (typeof(TElement) == typeof(string))
						{
							map.Element(m =>
							{
								m.Column(otherColumnName);
								m.Formula(formula);
								m.Type((IType)NHibernateUtil.AnsiString);
								m.Length(50);
							});
						} else if (typeof(TElement).IsEnum)
						{
                            map.Element(m =>
                            {
                                m.Column(otherColumnName);
                                m.Formula(formula);
								m.Type<EnumStringType<TElement>>();
                                m.Length(50);
                            });
                        }
						else
						{
							map.ManyToMany(m =>
							{
								m.Column(otherColumnName);
								m.Formula(formula);
							});
						}
					})
				);
			}
		}

		public static void MapBag<TEntity, TElement>(this ICollectionPropertiesContainerMapper<TEntity> mapper, PropertyInfo prop)
		{
			var notVisiblePropertyOrFieldName = prop.Name;
			var columnNameOrPropertyRef = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
			var manyToManyTableName = prop.GetCustomAttribute<TableAttribute>()?.Name;
			var otherColumnName = prop.GetCustomAttribute<ColumnAttribute>()?.OtherName;
			var cascade = prop.GetCustomAttribute<CascadeAttribute>()?.Cascade;
            var fetchMode = prop.GetCustomAttribute<FetchAttribute>()?.Mode;
            var lazy = prop.GetCustomAttribute<NotLazyAttribute>() != null ? false : true;
			var inverse = prop.GetCustomAttribute<InverseAttribute>() != null ? true : false;
			var formula = prop.GetCustomAttribute<FormulaAttribute>()?.Formula;

			columnNameOrPropertyRef = FuncHelper.FindReferencedColumnName(typeof(TEntity), typeof(TElement), columnNameOrPropertyRef);

			if (!string.IsNullOrEmpty(columnNameOrPropertyRef) && string.IsNullOrEmpty(manyToManyTableName) && string.IsNullOrEmpty(otherColumnName))
			{
				// One To Many
				if (typeof(TElement).IsValueType || typeof(TElement) == typeof(string))
					throw new Exception($"The property {columnNameOrPropertyRef} with OneToMany cannot contain simple type.");

				mapper.Bag(notVisiblePropertyOrFieldName, new Action<IBagPropertiesMapper<TEntity, TElement>>(map =>
				{
					map.Key(k => k.Column(columnNameOrPropertyRef));
					if (cascade.HasValue)
						map.Cascade(cascade.Value);
					map.Lazy(lazy ? CollectionLazy.Lazy : CollectionLazy.NoLazy);
					map.Inverse(inverse);
                    map.Fetch(FetchFactor.Get(fetchMode));
                }), action => action.OneToMany());
			}
			else if (!string.IsNullOrEmpty(manyToManyTableName))
			{
				// Many To Many
				if (string.IsNullOrEmpty(columnNameOrPropertyRef))
					columnNameOrPropertyRef = typeof(TEntity).Name + "Id";
				if (string.IsNullOrEmpty(otherColumnName))
					otherColumnName = typeof(TElement).Name + "Id";

				mapper.Bag(
					notVisiblePropertyOrFieldName,
					new Action<IBagPropertiesMapper<TEntity, TElement>>(map =>
					{
						map.Key(k => k.Column(columnNameOrPropertyRef));
						if (cascade.HasValue)
							map.Cascade(cascade.Value);
						map.Lazy(lazy ? CollectionLazy.Lazy : CollectionLazy.NoLazy);
						map.Inverse(inverse);
						map.Table(manyToManyTableName);
					}),
					new Action<ICollectionElementRelation<TElement>>(map =>
					{
						if (typeof(TElement) == typeof(string))
						{
							map.Element(m =>
							{
								m.Column(otherColumnName);
								m.Formula(formula);
								m.Type((IType)NHibernateUtil.AnsiString);
								m.Length(50);
							});
						} else if (typeof(TElement).IsEnum)
						{
                            map.Element(m =>
                            {
                                m.Column(otherColumnName);
                                m.Formula(formula);
                                m.Type<EnumStringType<TElement>>();
                                m.Length(50);
                            });
                        }
						else
						{
							map.ManyToMany(m =>
							{
								m.Column(otherColumnName);
								m.Formula(formula);
							});
						}
					})
				);
			}
		}

	}
}
