﻿using System;

namespace NHibernate.SimpleMapping.Interfaces
{
    public interface IEntity : IHaveId
    {
        /// <summary>
        /// Recommended length 150 characters
        /// </summary>
        string CreatedBy { get; set; }

        /// <summary>
        /// Recommended length 150 characters
        /// </summary>
        string UpdatedBy { get; set; }

        /// <summary>
        /// Recommended Utc DateTime
        /// </summary>
        DateTime CreatedDate { get; set; }

        /// <summary>
        /// Recommended Utc DateTime
        /// </summary>
        DateTime UpdatedDate { get; set; }

        int Version { get; set; }

        bool IsDeleted { get; }

        void SetDeleted(bool isDeleted = true);
    }
}
