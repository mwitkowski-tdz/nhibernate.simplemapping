﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NHibernate.SimpleMapping.Interfaces
{
    public interface IEncryptor
    {
        string Encrypt(string value);
        string Decrypt(string value);
    }
}
