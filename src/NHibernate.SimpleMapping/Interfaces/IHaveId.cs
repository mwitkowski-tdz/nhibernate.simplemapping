﻿namespace NHibernate.SimpleMapping.Interfaces
{
    public interface IHaveId
    {
        string Id { get; set; }
    }
}
