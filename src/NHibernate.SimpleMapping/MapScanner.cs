﻿using NHibernate.SimpleMapping.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NHibernate.SimpleMapping
{
	public static class MapScanner
	{
		/// <summary> Scans Assemblies and returns a list of types of ClassMapping<>. </summary> <param
		/// name="assemblies">Assemblies to scan</param> <returns>Returns a list of mapping types
		/// (SubclassMapping<TEntity> types)</returns>
		public static IEnumerable<System.Type> Scan(params Assembly[] assemblies)
		{
			return Scan(assemblies, false);
		}

		/// <summary> Scans Assemblies and returns a list of types of ClassMapping<>. </summary> <param
		/// name="assemblies">Assemblies to scan</param> <param name="useAssignedKeyGen">useAssignedKeyGen will replace
		/// your own key generator with NHibernate.Mapping.ByCode.Generators.Assigned. Useful for dedicated sessions to
		/// sync data between two databases.</param> <returns>Returns a list of mapping types (SubclassMapping<TEntity> types)</returns>
		public static IEnumerable<System.Type> Scan(Assembly[] assemblies, bool useAssignedKeyGen = false)
		{
			var classMappings = new List<System.Type>();

			foreach (var assembly in assemblies)
			{
				var entityTypes = assembly.GetTypes()
					.Where(x => x.IsClass && !x.IsInterface && x.GetCustomAttribute<TableAttribute>() != null)
					.ToList();

				foreach (var entityType in entityTypes)
				{
					var mapType = MapEntity(entityType, useAssignedKeyGen);
					if (mapType != null)
						classMappings.Add(mapType);
				}
			}
			return classMappings;
		}

		/// <summary> Generates a mapping type for a single entity type </summary> <param name="entityType">Type to
		/// map</param> <param name="useAssignedKeyGen"> useAssignedKeyGen will replace your own key generator with
		/// NHibernate.Mapping.ByCode.Generators.Assigned. Useful for dedicated sessions to sync data between two
		/// databases. </param> <returns>Returns a mapping type (eg. ClassMapping<TEntity> type)</returns>
		public static System.Type MapEntity(System.Type entityType, bool useAssignedKeyGen = false)
		{
			// Do not map if [NotMapped] attribute exists on the current class
			if (entityType.GetCustomAttribute<NotMappedAttribute>(false) != null)
				return null;

			// Do not map if [Table] attribute doesn't exist on the current class and all base classes
			if (entityType.GetCustomAttribute<TableAttribute>() == null)
				return null;

			// [Discriminator] attribute exists on the base class but doesn't exist on the current class
			if (entityType.GetCustomAttribute<DiscriminatorAttribute>() != null && entityType.GetCustomAttribute<DiscriminatorAttribute>(false) == null)
			{
				// [Table] attribute doesn't exist on the current class
				if (entityType.GetCustomAttribute<TableAttribute>(false) == null)
				{
					// SubclassMapping with Discriminator
					return typeof(SubMapBuilder<>).MakeGenericType(entityType);
				}
				else
				{
					// SubclassMapping with Discriminator and Join
					return typeof(SubMapJoinedBuilder<>).MakeGenericType(entityType);
				}
			}

			// [Table] attribute exists on the current class and on the base class (we assume they are different)
			if (entityType.GetCustomAttribute<TableAttribute>(false) != null && entityType.BaseType.GetCustomAttribute<TableAttribute>() != null)
			{
				// JoinedSubclassMapping
				return typeof(JoinedMapBuilder<>).MakeGenericType(entityType);
			}

			// Everything else with [Table] attribute on the current class or any base class
			if (useAssignedKeyGen)
				return typeof(MapBuilderWithAssignedKey<>).MakeGenericType(entityType);
			else
				return typeof(MapBuilder<>).MakeGenericType(entityType);
		}
	}
}
