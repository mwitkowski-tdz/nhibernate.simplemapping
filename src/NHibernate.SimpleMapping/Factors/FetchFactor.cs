﻿using NHibernate.Mapping;
using NHibernate.Mapping.ByCode;
using System;
using System.Collections.Generic;
using System.Text;

namespace NHibernate.SimpleMapping.Factors
{
    public static class FetchFactor
    {
        public static CollectionFetchMode Get(NHibernate.SimpleMapping.Enums.FetchMode? fetchMode)
        {
            if (fetchMode.HasValue)
            {
                switch (fetchMode.Value)
                {
                    case NHibernate.SimpleMapping.Enums.FetchMode.Join:
                        return CollectionFetchMode.Join;
                    case NHibernate.SimpleMapping.Enums.FetchMode.Select:
                        return CollectionFetchMode.Select;
                    case NHibernate.SimpleMapping.Enums.FetchMode.Subselect:
                        return CollectionFetchMode.Subselect;
                }
            }
            return CollectionFetchMode.Select;
        }
        
    }
}
