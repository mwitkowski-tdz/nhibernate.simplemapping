﻿using NHibernate.Engine;
using NHibernate.Id;
using NHibernate.Mapping.ByCode;
using System.Threading;
using System.Threading.Tasks;

namespace NHibernate.SimpleMapping.Generators
{
    public class IntegerValueGenerator : IIdentifierGenerator
    {
        public object Generate(ISessionImplementor session, object obj)
        {
            return IdentityGenerator.RandomInt();
        }

        public Task<object> GenerateAsync(ISessionImplementor session, object obj, CancellationToken cancellationToken)
        {
            return Task.Run(() => Generate(session, obj), cancellationToken);
        }
    }

    public class IntegerValueGeneratorDef : IGeneratorDef
    {
        public string Class => typeof(IntegerValueGenerator).AssemblyQualifiedName;

        public object Params => null;

        public System.Type DefaultReturnType => typeof(int);

        public bool SupportedAsCollectionElementId => true;
    }
}
