﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NHibernate.SimpleMapping.Enums
{
    public enum FetchMode
    {
        Select,
        Join,
        Subselect
    }
}
