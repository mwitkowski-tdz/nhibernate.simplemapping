﻿using NHibernate.SimpleMapping.Attributes;
using NHibernate.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NHibernate.SimpleMapping
{
    internal static class FuncHelper
    {
        public static IType GetNHibernateType(System.Type propType, PropertyInfo prop)
        {
            if (propType.IsEnum)
                return (IType)Activator.CreateInstance(typeof(EnumStringType<>).MakeGenericType(propType));

            switch (propType.Name)
            {
                case "String":
                    return prop.GetCustomAttribute<AnsiAttribute>() != null ? (IType)NHibernateUtil.AnsiString : NHibernateUtil.String;

                case "Int16":
                    return NHibernateUtil.Int16;

                case "Int32":
                    return NHibernateUtil.Int32;

                case "Int64":
                    return NHibernateUtil.Int64;

                case "Boolean":
                    return NHibernateUtil.Boolean;

                case "Decimal":
                    return NHibernateUtil.Decimal;

                case "Double":
                    return NHibernateUtil.Double;

                case "Guid":
                    return NHibernateUtil.Guid;

                case "TimeSpan":
                    return NHibernateUtil.TimeSpan;

                case "DateTimeOffset":
                case "DateTime":
                    if (prop.GetCustomAttribute<UtcDateTimeAttribute>() != null)
                    {
                        return prop.GetCustomAttribute<UtcDateTimeAttribute>().NoMs ? (IType)NHibernateUtil.UtcDateTimeNoMs : NHibernateUtil.UtcDateTime;
                    }
                    else if (prop.GetCustomAttribute<DateTimeAttribute>() != null)
                    {
                        return prop.GetCustomAttribute<DateTimeAttribute>().NoMs ? (IType)NHibernateUtil.DateTimeNoMs : NHibernateUtil.DateTime;
                    }
                    else if (prop.GetCustomAttribute<DateAttribute>() != null)
                    {
                        return NHibernateUtil.Date;
                    }
                    else
                    {
                        return NHibernateUtil.DateTime;
                    }
                default:
                    throw new NotSupportedException($"Type '{propType.Name}' not supported");
            }
        }

        public static int GetNHibernateLength(System.Type propType, PropertyInfo prop, int? defaultLength = null)
        {
            if (prop.GetCustomAttribute<LengthAttribute>() != null)
            {
                return prop.GetCustomAttribute<LengthAttribute>().Max;
            }
            else if (defaultLength.HasValue)
            {
                return defaultLength.Value;
            }

            if (propType.IsEnum)
            {
                return 50;
            }

            if (propType == typeof(string))
            {
                return 4000;
            }
            else
            {
                return 0;
            }
        }

        public static object GetNHibernateDefaultValue(System.Type propType, PropertyInfo prop, bool isString)
        {
            var defaultValue = prop.GetCustomAttribute<DefaultAttribute>()?.DefaultValue;

            if (defaultValue == null)
                return null;

            if (isString)
                return defaultValue;

            if (propType == typeof(bool))
                return Convert.ToBoolean(defaultValue) ? 1 : 0;
            else
                return Convert.ChangeType(defaultValue, propType);
        }

        public static bool IsGenericTypeOf(System.Type type, System.Type genericTypeDefinition)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            if (genericTypeDefinition == null)
                throw new ArgumentNullException("genericTypeDefinition");

            var typeTest = new Predicate<System.Type>(i => i.IsGenericType && i.GetGenericTypeDefinition() == genericTypeDefinition);

            return (typeTest(type)) || type.GetInterfaces().Any(i => typeTest(i));
        }

        public static IEnumerable<PropertyInfo> GetPublicVirtualProperties(System.Type type, bool excludeInherited = false)
        {
            var flags = excludeInherited ? BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly : BindingFlags.Instance | BindingFlags.Public;
            return type.GetProperties(flags).Where(x => x.CanRead && x.CanWrite && x.GetMethod.IsVirtual);
        }

        public static string FindReferencedColumnName(System.Type entityType, System.Type elementType, string columnNameOrPropertyRef)
        {
            PropertyInfo property = null;

            // Find Property
            if (string.IsNullOrEmpty(columnNameOrPropertyRef))
            {
                var matchedProps = FuncHelper.GetPublicVirtualProperties(elementType).Where(x => x.PropertyType == entityType).ToArray();
                if (matchedProps.Length == 1)
                    property = matchedProps[0];
            }
            else
            {
                property = elementType.GetProperty(columnNameOrPropertyRef);
            }

            if (property != null)
            {
                if (property.GetCustomAttribute<ColumnAttribute>() != null)
                {
                    columnNameOrPropertyRef = property.GetCustomAttribute<ColumnAttribute>().Name;
                }
                else
                {
                    columnNameOrPropertyRef = property.Name + "Id";
                }
            }

            return columnNameOrPropertyRef;
        }

        public static bool TryParseEnum<TEnum>(string str, out TEnum result)
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException($"Cannot parse the type {typeof(TEnum).Name} to enum. This type must be an enum type.");

            try
            {
                result = (TEnum)Enum.Parse(typeof(TEnum), str, true);
                return true;
            }
            catch
            {
                result = default;
                return false;
            }
        }
    }
}
