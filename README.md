# NHibernate.SimpleMapping

This project inspired by need of having no mapping at all similar to Entity Framework functionality. It uses its own extra attributes to describe the model. The mapping system is not intended to generate the database structure. Instead you must use [FluentMigrator](https://fluentmigrator.github.io/). We are aware there is MappingAttributes project [NHibernate.Mapping.Attributes](https://nhibernate.info/doc/nh/en/index.html#mapping-attributes) but it is not used due to complexity.

The main goal of the project is to simplify the nHibernate mapping process by using attributes against entities and its properties. Apply attributes only if neccessery. You can still use other mapping techniques along with `SimpleMapping` if you need to.

If you want to be a contributor just create an issue in the project and ask us to add you to the Team ;-). Or just fork the repository and come with your PR and suggestions.

# How to install

Just add the following nuget to your project: [NHibernate.SimpleMapping](https://www.nuget.org/packages/NHibernate.SimpleMapping/)

# How to use

Please note all attributes are optional except of `[Table]` against classes. You can also use `[Table]` or `[Column]` attributes to describe `one-to-many`, `many-to-one` and `many-to-many` relations if table/column name is different from default one (Property + Id). 

> PLEASE DO NOT MESS UP SimpleMapping ATTRIBUTES WITH DataAnnotations ATTRIBUTES.

Default nvarchar string length is 4000. All nullable types or classes or strings expect nullable column by default otherwise the column is not nullable. Entities are lazy by default. Properties are not lazy by default. References and Bags are lazy by default.

Feel free to use `NHibernate.Cfg.ImprovedNamingStrategy` if you are with `PostgreSql` to make column names snake-case like.

The mapping logic as follows:
```
if ([NotMapped] attribute exists on the current entity class)
    Exit;

if ([Table] attribute doesn't exist on the current entity class and all base classes)
    Exit;

if ([Discriminator] attribute exists on the base class but doesn't exist on the current class)
{
    if ([Table] attribute doesn't exist on the current class)
    {
        Map SubclassMapping with Discriminator;
        Exit;
    }
    else
    {
        Map SubclassMapping with Discriminator and Join;
        Exit;
    }
}

if ([Table] attribute exists on the current class and on the base class (we assume they are different))
{
    Map JoinedSubclassMapping;
    Exit;
}

Map ClassMapping;
Exit;
```

See the advanced example bellow:

```
using NHibernate.SimpleMapping;
using NHibernate.SimpleMapping.Attributes;

[Table("contacts")]
public class Contact
{
    // Id column. You can use any NH generators like `NHibernate.Mapping.ByCode.AssignedGeneratorDef` or create your own ones.
    [Key(typeof(NHibernate.SimpleMapping.Generators.WebHashValueGeneratorDef))]
    [Ansi]
    [Length(16)]
    public virtual string Id { get; set; }

    // String Length is 150 nvarchar. Column is not nullable.
    [NotNull]
    [Length(150)]
    public virtual string Name { get; set; }

    // Default String Length is 4000 characters. Make property lazy.
    [Lazy]
    public virtual string Description { get; set; }
    
    // Default String Length is 8000 characters.
    [AnsiString]
    public virtual string Configuration { get; set; }

    // nvarchar(max)
    [Length(100000)]
    public virtual string FullDescription { get; set; }

    // All virtual properties are mapped based on their type and nullability
    public virtual int Counter { get; set; }

    [UtcDateTime(NoMs = true)]
    public virtual DateTime CreatedDate { get; set; }

    [Date]
    public virtual DateTime DayOfBirth { get; set; }

    // This property won't be mapped as it is not virtual. You will actually have an exception thrown by hibernate if you are using proxies.
    public string Custom1 { get; set; }

    // This property won't be mapped because of attribute [NotMapped]
    [NotMapped]
    public virtual string Custom2 { get; set; }

    // This will be varchar(50) by default because of the enum. You can assign a different string length if you need. No support for numeric enums yet.
    public virtual GenderEnum Gender { get; set; }

    // Semicolon separated list of enums or strings stored in a single column. You can use IList too. Use [Length] to change the length which by default is 2000. Enum uses Ansi string by default.
    public virtual ISet<GenderEnum> Genders { get; set; }

    ...
    public enum GenderEnum
    {
        Male,
        Female,
    }
    ...

    // Because bool type is not nullable itself the mapping will be applied as not nullable too. Same for other struct types.
    [Default("false")]
    public virtual bool Archived { get; set; }

    // This property will be readonly.
    [ReadOnly] // Only `where` queries will apply. (Eg. `select * from [contacts] where name + surname = 'John Lennon'`)
	[Formula("name + surname")]
    public virtual string FullName { get { return Name + Surname; } }

    // Reference (many-to-one). It expects the column `Contact.AddressId` by default. You can use [Column("my_custom_column")] if you need to map a different column name.
    [Cascade(Cascade.All | Cascade.DeleteOrphans)]
    public virtual Address Address { get; set; }
    
    // Bag (one-to-many). By default it expects the column `Order.ContactId`. You can use [Column("my_custom_column")] if you need to map a different column name.
    // `ICollection<>` describes Bag. You can also use `IList<>` to describe List or `ISet<>` to describe Set.
    public virtual ICollection<Order> Orders { get; set; }

    // Bag (many-to-many). The attribute [Table] is required. You can also use [Column("my_custom_column", OtherName = "column_from_the_other_side")] if you need to map a different column name.
    [Table("contacts_orders")]
    public virtual ICollection<Order> AllOrders { get; set; }

    // Enum/String element collection mapping (it could also be ICollection<string>). You can also use IList or ISet.
    [Table("contact_lastorder_states")]
    [Column("contact_id", OtherName = "state_value")]
    public virtual ICollection<StateEnum> AllLastOrderStates { get; set; }

    // The Order entity mapped in the traditional way (hbm.xml or map-by-code)
    [NotLazy]
    public virtual Order LastOrder { get; set; }

	// ManyToOne as OneToOne relation because of [Unique]. It requires referenced column RelatedUserId in this table
	[Unique] // Unique attribute is required for OneToOne relationship
    public virtual User RelatedUser { get; set; }

    ...
	[Table("users")]
	public class User
	{
		// OneToOne backend property. Referenced column is on the other side. See Contact.RelatedUser above.
		[OneToOne]
		public virtual Contact RelatedContact { get; set; }
	}
    ...
}

[Table("addresses")]
public class Address
{
    [Key(typeof(NHibernate.Mapping.ByCode.AssignedGeneratorDef))]
    public virtual string Id { get; set; }

    [Length(150)]
    public virtual string Street { get; set; }

    [Length(150)]
    public virtual string City { get; set; }
}

// Mapped in the traditional way (hbm.xml or map-by-code) but still able to be referenced by SimpleMapping entities. It is not applied to inherited [NotMapped] attributes.
[Table("orders")]
[NotMapped]
public class Order
{
    // Properties are not mapped by `SimpleMapping` attributes because of [NotMapped] against class
    // So you can use any different mapper with this class (like FluentNHibernate or MapByCode).
    // ...
}

// [Discriminator] allows to have many classes in one table. Use [Discriminator("class_type")] to have a different discriminator column name.
[Table("dogs")]
[Discriminator]
public abstract class Dog
{
    // ...
}
// So you don't have to worry about [Table] as it is already applied to the base class. It will map a column `discriminator` and put "Doberman" as a discriminated type name. Similar to `SubclassMapping<Doberman>`.
public class Doberman : Dog
{
    // ...
}

// Another example with no discriminator but with table class
[Table("dogs")]
public class Dog
{
    // ...
}
// These are two tables mapped by using `JoinedSubclassMapping`. One table is `dogs` and another is `dobermans`.
[Table("dobermans")]
public class Doberman : Dog
{
    // ...
}

// How to setup the mapping in asp.net.core application
public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        var cfg = new Configuration();
        // Some nhibernate config ...
        var mapper = new ModelMapper();
        mapper.AddMappings(MapScanner.Scan(typeof(Startup).Assembly)); // You can add more assemblies here
        var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
        cfg.AddDeserializedMapping(mapping, "MySimpleMapping");
        
        // NH Configuration
        services.AddSingleton(cfg);
        // ISessionFactory
        services.AddSingleton(s => s.GetRequiredService<Configuration>().BuildSessionFactory());
        // ISession
        services.AddScoped(s => s.GetRequiredService<ISessionFactory>().OpenSession());
    }
}

// And then use it in normal way
public class MyController : Controller
{
    private readonly ISession _session;

    public MyController(ISession session)
    {
        _session = session;
    }

    public async Task<OrderDto> GetOrder(string id)
    {
        var order = await _session.GetAsync<Order>(id);
        var orderDto = new OrderDto(order);
        return orderDto; // Never return entities to the client :-).
    }
}

```
If you still have any questions please create an issue.

We wish everyone successful results with projects.
